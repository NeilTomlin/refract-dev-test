# Refract Dev Test


## Execution

Clone this repository and run `npm start` to open a localhost browser window with demonstrable web page with nav and menu button


## Notes on Incomplete status

Tasks not complete or fully complete as per requirements:
- timeout / simulated AJAX
- css loading animation
- modal dialog open/closing
- reviewing all modern browsers


## Assumptions


- You wanted to see demonstrated a javascript approach independent of frameworks to isolate developers without a fundamental understanding of how the javascript layer interacts with the DOM
- Full completion of the task was not necessary to demonstrate this or expected



## My Thoughts

The scope of the task is too wide; it's unclear exactly what you are expecting the successul candidate to demonstrate.
> a reusable pattern, instead of a flat set of event bind calls. 

I have included a basic project to create an unoptimised development environment to facilitate code build tasks that would be required to optimise browser consistency and productionise the code. Although it was not explicit in the document I received but my understanding of the above requirement seemed to suggest a modular Javascript approach if no framework was permitted. I have not attempted to develop in this way prior to this test and expect there is much scope for improvement in my understanding of it.


## Suggestions

- Provide a skeleton project (unless of course this is within scope of the test, if so this should be explicit)
- Reduce the scope of the test or have a "hands on" 1.5hour paired programming session with a Refract developer
- Provide typed version of code snippet, not an image


## Further Notes

I really enjoyed meeting Rich and Abishek and the dev culture seems spot on. I would be very happy to elaborate on what improvements I would make to my submission were I to have the time in order to create code which is "production ready," this is very much draft code and not at all what I would expect of myself to add to a PR for production code but given time constraints and progression with other applications, I am unable to dedicate any further time to my submission.

