(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Menu = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Menu = /*#__PURE__*/function () {
  function Menu(linkList) {
    _classCallCheck(this, Menu);

    this.linkList = linkList;
    this.linkListOpen = false;
  }

  _createClass(Menu, [{
    key: "getMenu",
    value: function getMenu() {
      var _this = this;

      var menuIcon = document.createElement("button");
      var imgElement = document.createElement("img");
      imgElement.setAttribute("src", "/icons/menu.svg");
      imgElement.setAttribute("height", "32");
      imgElement.setAttribute("width", "32");
      menuIcon.appendChild(imgElement);
      menuIcon.classList.add("menu-button", "action-icon");
      menuIcon.addEventListener('click', function (e) {
        e.preventDefault();

        _this.toggleLinkList();
      });
      console.log(menuIcon);
      return menuIcon;
    }
  }, {
    key: "toggleLinkList",
    value: function toggleLinkList() {
      var _this2 = this;

      this.linkListOpen = !this.linkListOpen;

      if (this.linkListOpen) {
        var linkListElement = document.createElement("ul");
        linkListElement.classList.add("menu-open");

        if (this.linkList.manage) {
          linkListElement.appendChild(this.createNavLinkElement("Manage collections and tasks", function () {
            return _this2.openManageModal;
          }));
        }

        document.querySelector(".navbar").appendChild(linkListElement);
      }
    }
  }, {
    key: "createNavLinkElement",
    value: function createNavLinkElement(text, callback) {
      var listItemElement = document.createElement("li");
      listItemElement.appendChild(document.createTextNode(text));
      listItemElement.addEventListener("click", callback());
      return listItemElement;
    }
  }, {
    key: "openManageModal",
    value: function openManageModal() {
      var manageModalElement = document.createElement("div");
      var manageModalHeading = document.createElement("p");
      var manageModalButton = document.createElement("button");
      manageModalElement.classList.add("manage-modal");
      manageModalButton.appendChild(document.createTextNode("Process"));
      manageModalButton.addEventListener("click", function () {
        alert("Run provided code snippet");
      });
      manageModalElement.appendChild(manageModalHeading);
      manageModalElement.appendChild(manageModalButton);
      document.body.appendChild(manageModalElement);
    }
  }]);

  return Menu;
}();

exports.Menu = Menu;

},{}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.navbar = void 0;

var _menu = require("./menu");

var _photo = require("./photo");

var _search = require("./search");

//
// Navbar
// --------------------------------------------------
var navbar = function navbar(pageTitle, navOptions) {
  var navElement = document.createElement("nav");
  var pageTitleElement = document.createElement("h1");
  var navActionsWrapper = document.createElement("div");
  var navMenu = new _menu.Menu(navOptions.menu);
  var navMenuElement = navMenu.getMenu();
  navElement.classList.add("navbar");
  pageTitleElement.classList.add("page-title");
  navActionsWrapper.classList.add("action-wrapper"); // Add Search

  navOptions.search && navElement.appendChild((0, _search.search)()); // Add Page Heading

  pageTitleElement.appendChild(document.createTextNode(pageTitle));
  navElement.appendChild(pageTitleElement); // Add Menus

  navOptions.photo && navActionsWrapper.appendChild((0, _photo.photo)());
  navActionsWrapper.appendChild(navMenuElement);
  navElement.appendChild(navActionsWrapper);
  return navElement;
};

exports.navbar = navbar;

},{"./menu":1,"./photo":3,"./search":4}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.photo = void 0;

var photo = function photo() {
  var photoIcon = document.createElement("a");
  photoIcon.setAttribute("href", "/photo");
  photoIcon.classList.add("action-icon");
  var imgElement = document.createElement("img");
  imgElement.setAttribute("src", "/icons/photo.svg");
  imgElement.setAttribute("height", "32");
  imgElement.setAttribute("width", "32");
  photoIcon.appendChild(imgElement);
  return photoIcon;
};

exports.photo = photo;

},{}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.search = void 0;

var search = function search() {
  var searchField = document.createElement("input");
  searchField.setAttribute("type", "text");
  searchField.setAttribute("placeholder", "Find people or topics");
  var inputWrapper = document.createElement("div");
  inputWrapper.classList.add("nav-search");
  inputWrapper.appendChild(searchField);
  return inputWrapper;
};

exports.search = search;

},{}],5:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Page = void 0;

var _navbar = require("../components/nav/navbar");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Page = /*#__PURE__*/function () {
  function Page(pageTitle, navOptions) {
    _classCallCheck(this, Page);

    this.pageTitle = pageTitle;
    this.navOptions = navOptions;
  }

  _createClass(Page, [{
    key: "getPage",
    value: function getPage() {
      var pageElement = document.createElement("div");
      pageElement.classList.add("page-wrapper");
      var navbarElement = (0, _navbar.navbar)(this.pageTitle, this.navOptions);
      pageElement.appendChild(navbarElement);
      return pageElement;
    }
  }]);

  return Page;
}();

exports.Page = Page;

},{"../components/nav/navbar":2}],6:[function(require,module,exports){
"use strict";

var _home = require("./pages/home");

//
// Main
// --------------------------------------------------
document.body.appendChild(_home.homePageElement);

},{"./pages/home":7}],7:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.homePageElement = void 0;

var _page = require("../components/page");

//
// Home
// --------------------------------------------------
var pageTitle = "Home";
var navOptions = {
  search: true,
  photo: true,
  menu: {
    manage: true,
    logout: false
  }
};
var homePage = new _page.Page(pageTitle, navOptions);
var homePageElement = homePage.getPage();
exports.homePageElement = homePageElement;

},{"../components/page":5}]},{},[6]);
