module.exports = function (grunt) {

    // Display the elapsed execution time of Grunt tasks
    require("time-grunt")(grunt);

    // Load Grunt tasks
    require("load-grunt-tasks")(grunt);

    // Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),

        browserify: {
            build: {
                options: {
                    browserifyOptions: {
                        debug: false
                    },
                    transform: [
                        [
                            "babelify", {
                                "presets": [
                                    "@babel/preset-env"
                                ],
                                "targets": "> 0.25%, not dead"
                            }
                        ]
                    ]
                },
                src: [
                    "src/js/main.js"
                ],
                dest: "public/js/index.js"
            }
        }
    });

    // Build task
    grunt.registerTask("build", [
        "browserify"
    ]);

    // Default task
    grunt.registerTask("default", "build");

};
