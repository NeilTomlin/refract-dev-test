import { navbar } from '../components/nav/navbar';

export class Page {
    constructor(pageTitle, navOptions) {
        this.pageTitle = pageTitle;
        this.navOptions = navOptions
    }

    getPage() {
        const pageElement = document.createElement("div");
        pageElement.classList.add("page-wrapper");
        const navbarElement = navbar(this.pageTitle, this.navOptions)
        pageElement.appendChild(navbarElement)

        return pageElement;

    }
}