//
// Navbar
// --------------------------------------------------
import { Menu } from "./menu";
import { photo } from "./photo";
import { search } from "./search";

const navbar = (pageTitle, navOptions) => {
    const navElement = document.createElement("nav");
    const pageTitleElement = document.createElement("h1");
    const navActionsWrapper = document.createElement("div");
    const navMenu = new Menu(navOptions.menu);
    const navMenuElement = navMenu.getMenu();
    navElement.classList.add("navbar");
    pageTitleElement.classList.add("page-title");
    navActionsWrapper.classList.add("action-wrapper")

    // Add Search
    navOptions.search && navElement.appendChild(search());
    // Add Page Heading
    pageTitleElement.appendChild(document.createTextNode(pageTitle));
    navElement.appendChild(pageTitleElement);
    // Add Menus
    navOptions.photo && navActionsWrapper.appendChild(photo());
    navActionsWrapper.appendChild(navMenuElement);
    navElement.appendChild(navActionsWrapper);

    return navElement;
}

export { navbar };
