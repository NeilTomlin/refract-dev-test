export class Menu {
    constructor(linkList) {
        this.linkList = linkList;
        this.linkListOpen = false;
    }

    getMenu() {
        const menuIcon = document.createElement("button");
        const imgElement = document.createElement("img");

        imgElement.setAttribute("src", "/icons/menu.svg");
        imgElement.setAttribute("height", "32");
        imgElement.setAttribute("width", "32");

        menuIcon.appendChild(imgElement);
        menuIcon.classList.add("menu-button", "action-icon");
        menuIcon.addEventListener('click', e => {
            e.preventDefault();
            this.toggleLinkList()
        });

        console.log(menuIcon);
        return menuIcon;
    }

    toggleLinkList() {
        this.linkListOpen = !this.linkListOpen;
        if (this.linkListOpen) {
            const linkListElement = document.createElement("ul")
            linkListElement.classList.add("menu-open");

            if (this.linkList.manage) {
                linkListElement.appendChild(this.createNavLinkElement("Manage collections and tasks", () => this.openManageModal));
            }

            document.querySelector(".navbar").appendChild(linkListElement);


        }
    }

    createNavLinkElement(text, callback) {
        const listItemElement = document.createElement("li")

        listItemElement.appendChild(document.createTextNode(text));
        listItemElement.addEventListener("click", callback())
        return listItemElement;
    }

    openManageModal() {
        const manageModalElement = document.createElement("div");
        const manageModalHeading = document.createElement("p");
        const manageModalButton = document.createElement("button");

        manageModalElement.classList.add("manage-modal");
        manageModalButton.appendChild(document.createTextNode("Process"));
        manageModalButton.addEventListener("click", () => { alert("Run provided code snippet") });

        manageModalElement.appendChild(manageModalHeading);
        manageModalElement.appendChild(manageModalButton);
        document.body.appendChild(manageModalElement);
    }

}
