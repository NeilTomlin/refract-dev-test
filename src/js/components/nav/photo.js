export const photo = () => {
    const photoIcon = document.createElement("a");
    photoIcon.setAttribute("href", "/photo");
    photoIcon.classList.add("action-icon");

    const imgElement = document.createElement("img");
    imgElement.setAttribute("src", "/icons/photo.svg");
    imgElement.setAttribute("height", "32");
    imgElement.setAttribute("width", "32");

    photoIcon.appendChild(imgElement);

    return photoIcon;
}