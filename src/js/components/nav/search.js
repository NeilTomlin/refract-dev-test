export const search = () => {
    const searchField = document.createElement("input");
    searchField.setAttribute("type", "text");
    searchField.setAttribute("placeholder", "Find people or topics");

    const inputWrapper = document.createElement("div");
    inputWrapper.classList.add("nav-search");
    inputWrapper.appendChild(searchField);

    return inputWrapper;
}