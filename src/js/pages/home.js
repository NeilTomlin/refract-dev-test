//
// Home
// --------------------------------------------------
import { Page } from '../components/page';
const pageTitle = "Home";
const navOptions = { search: true, photo: true, menu: { manage: true, logout: false } }

const homePage = new Page(pageTitle, navOptions)
const homePageElement = homePage.getPage();

export { homePageElement }
